define([
  'backbone'
],function(
  backbone
){
  // User model
  var User = Backbone.Model.extend(),

  // People colleciton
      People = Backbone.Collection.extend({
        model: User
      }),

  // User data
      userData = [
                  {
                    name: 'John',
                    lname: 'Doe'
                  },
                  {
                    name: 'Mark',
                    lname: 'Doe'
                  }
                 ];

  userModelInstance = new User({name: 'Grace', lname: 'Poe'});

  // User model view
  var UserView = Backbone.View.extend({
        initialize: function() {
          this.model.on('change', this.render, this); 
        },
        template: _.template('<%= name %> <%= lname%>'),
        render: function() {
          var tmpl = this.template(this.model.toJSON());
          
          this.$el.html(tmpl);

          return this;
        },

        events: {
          'click': 'showUser'
        },

        showUser: function() {
          var newName = prompt('Enter new name');

          this.model.set('lname', newName)
        }
      });

  _people = new People(userData);
  _userModelView = new UserView({model: userModelInstance});
  $('body').append(_userModelView.render().el);
});
