define([
  // Libraries
  'underscore',
  'backbone',

  // Modules
  'modules/users/user'
],function(
  // Libraries
  _,
  backbone,

  // Modules
  user
) {
  return Backbone.Router.extend({
    initialize: function() {
        
    },

    routes: {
      '': 'index',
      'user': 'user'
    },

    index: function() {
      console.log('index page');
    },

    user: function() {
      console.log(user);
    }
  });
});
