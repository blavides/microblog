(function () {
  var root = this,
      publicLib = '/public/js/lib/';
 
  require.config({
    baseUrl: '/public/js',
    paths: {
      underscore: publicLib + 'underscore',
      backbone: publicLib + 'backbone',
      jquery: publicLib + 'jquery',
      text: publicLib + 'text'
    },
    shim: {
      underscore: {exports: '_'},
      backbone: {deps: ['underscore', 'jquery'], exports: 'Backbone'}
    }
  });
 
  require(['app'], function (app) {
    new app();

    Backbone.history.start({pushState: true});

	  $(document).on('click', 'a[href]:not([data-bypass])', function(evt){
   	  // Get the absolute anchor href.
	    var href = { prop: $(this).prop('href'), attr: $(this).attr('href') },

      // Get the absolute root.
          root = location.protocol + '//' + location.host;

      // Ensure the root is part of the anchor href, meaning it's relative.
      if (href.prop.slice(0, root.length) === root) {
        evt.preventDefault();

        Backbone.history.navigate(href.attr, true);
      }
    });
  });
})();
