({
  baseUrl: ".",
  paths: {
    require: "lib/require",
    underscore: "lib/underscore",
    jquery: "lib/jquery",
    backbone: "lib/backbone",
    text: "lib/text"
  },
  uglify: {
    no_mangle: false  
  },
  optimize: "none",
  name: "main",
  out: "built.js"
})
